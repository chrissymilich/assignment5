#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    return r


@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books, len = len(books))
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        new_name = request.form['name']
        books.append({'title' : new_name, 'id' : len(books) + 1})
        return redirect(url_for('showBook'))
    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        new_name = request.form['name']
        del books[book_id - 1]
        books.insert(book_id - 1, {'title' : new_name, 'id' : book_id})
        return redirect(url_for('showBook'))
    return render_template('editBook.html')
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        i = book_id - 1
        del books[i]
        while i < len(books) :
            books[i]['id'] = i + 1
            i += 1
        return redirect(url_for('showBook'))
    return render_template('deleteBook.html', books = books, book_id = book_id)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

